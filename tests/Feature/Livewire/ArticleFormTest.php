<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\ArticleForm;
use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Mail\Mailables\Content;
use Livewire\Livewire;
use Tests\TestCase;

//ESTOS TESTS PASAN

class ArticleFormTest extends TestCase
{
    use RefreshDatabase; //para que cree una BD en memoria y no nos llene la BD que usamos en el navegador
    
    
    function gests_cannot_create_or_update_articles()//funciona
    {
        $this->get(route('articles.create'))->assertRedirect('login');

        $article = Article::factory()->create();
        $this->get(route('articles.edit', $article))->assertSeeLivewire('article-form');
    }


    /**
     * @test
     */
    function article_form_renders_properly()//funciona
    {
      $user = User::factory()->create();

        $this->actingAs($user)->get(route('articles.create'))
        ->assertSeeLivewire('article-form');

        $article = Article::factory()->create();

        $this->actingAs($user)->get(route('articles.edit', $article))
        ->assertSeeLivewire('article-form');


       
        
    }

    /**
     * @test
     */
    function blade_template_is_wired_properly()//funciona
    {
        Livewire::test('article-form')
       
        ->assertSeeHtml('wire:submit.prevent="save"')
        ->assertSeeHtml('wire:model="article.title"')

        ->assertSeeHtml('wire:model="article.slug"')

        ->assertSeeHtml('wire:model="article.content"') 
        ;
    }
     /**
      * @test
      */
     
     function can_create_new_articles() //asi funciona

   
     {
      
      $user = User::factory()->create();

      //con la siguiente línea se inicializa el componente:
        Livewire::actingAs($user)->test('article-form')
       
     
         // "set" verifica o setea la propiedad del componente
        ->set('article.title', 'New article')
        
        ->set('article.slug', 'new-article')

        ->set('article.content', 'Article content')

        // "call"  ejecuta métodos
        ->call('save')

        // assertSessionHass verifica la llave en la sesion, en este caso la llave es"status"
        ->assertSessionHas('status')

        //assertRedirect vetrifica que nos redireccione a la ruta establecida, en este caso 'articles.index' 
        ->assertRedirect(route('articles.index'))
        ;

        //la siguiente linea verifica que en la BD se ha creado el artículo, le especifico que se debe crear en la tabla 'articles'
        //con eltítulo, el contenido y los atributos que he declarado en los "->set"
    
        $this->assertDatabaseHas('articles', [
            'title'=> 'New article',
            'slug'=> 'new-article',
            'content'=> 'Article content',
            'user_id'=> $user->id
        ]);

        //PARA EJECUTAR EL TEST, LO HAGO DESDE LA TERMINAL CON EL COMANDO: php artisan test --filter Y EL NOMBRE DEL MÉTODO,
        //EN ESTE CASO "can_create_new_articles": 
     }
    

     //-----------tests para las reglas ce validación--------------

     /**
      * @test
      */


     function title_is_required()//funciona
     {
        Livewire::test('article-form')
        
        ->set('article.content', 'Article content')
        ->call('save')
        ->assertHasErrors(['article.title'=>'required'])
        ->assertSeeHtml(__('validation.required', ['attribute' => 'title']))
        ;
     }

     
      /**
      * @test
      */


      function slug_is_required()//funciona
      {
         Livewire::test('article-form')
         
         ->set('article.title', 'New Article')
         ->set('article.slug', null)
         ->set('article.content', 'Article content')
         ->call('save')
         ->assertHasErrors(['article.slug'=>'required'])
         ->assertSeeHtml(__('validation.required', ['attribute' => 'slug']))
         ;
      }
    
   

        /**
      * @test
      */


      function slug_must_be_unique()//funciona
      {
         $article = Article::factory()->create();

         Livewire::test('article-form')
         
         ->set('article.title', 'New Article')
         ->set('article.slug', $article -> slug)
         ->set('article.content', 'Article content')
         ->call('save')
         ->assertHasErrors(['article.slug'=>'unique'])
         ->assertSeeHtml(__('validation.unique', ['attribute' => 'slug']))
         ;
      }


         /**
      * @test
      */


      function slug_must_only_contain_letters_numbers_dashes_and_underscores()//funciona
      {
         

         Livewire::test('article-form')
         
         ->set('article.title', 'New Article')
         ->set('article.slug', 'new -article$%^')
         ->set('article.content', 'Article content')
         ->call('save')
         ->assertHasErrors(['article.slug'=>'alpha_dash'])
         ->assertSeeHtml(__('validation.alpha_dash', ['attribute' => 'slug']))
         ;
      }

        /**
      * @test
      */


      function unique_rule_should_be_ignored_when_updating_the_same_slug()//funciona
      {
         $article = Article::factory()->create();

         $user = User::factory()->create();

         Livewire::actingAs($user)->test('article-form', ['article' => $article])
         
         ->set('article.title', 'New Article')
         ->set('article.slug', $article -> slug)
         ->set('article.content', 'Article content')
         ->call('save')
         ->assertHasNoErrors(['article.slug'=>'unique'])
         
         ;
      }
     
     function title_must_be_4_characters_min()//funciona
     {
      
        Livewire::test('article-form')
        ->set('article.title', 'Art')
        ->set('article.content', 'Article content')
        ->call('save')
        ->assertHasErrors(['article.title'=>'min'])
        ->assertSeeHtml(__('validation.min.string', ['attribute' => 'title', 'min' => 4]))

        ;
     }

     /**
      * @test
      */
     function content_is_required()//funciona
     {
        Livewire::test('article-form')
        ->set('article.title', 'New Article')
        ->call('save')
        ->assertHasErrors(['article.content'=>'required'])
        ->assertSeeHtml(__('validation.required', ['attribute' => 'content']))

        ;
     }

     /**
      * @test
      */
     function real_time_validation_works_for_title()//funciona
     {
        Livewire::test('article-form')
        ->set('article.title', '')
        ->assertHasErrors(['article.title' => 'required'])
        ->set('article.title', 'New')
        ->assertHasErrors(['article.title' => 'min'])
        ->set('article.title', 'New article')
        ->assertHasNoErrors('article.title')
        ;
     }

     /**
      * @test
      */
     function real_time_validation_works_for_content()//funciona
     {
        Livewire::test('article-form')
        ->set('article.content', '')
        ->assertHasErrors(['article.content' => 'required'])
        ->set('article.content', 'Article content')
        ->assertHasNoErrors('article.content')
        ;
     }

       //-----------tests para editar o actualizar--------------
        /**
         * @test
         */
       function can_update_articles()  //funciona
       {
            $article = Article::factory()->create();

            $user = User::factory()->create();

            Livewire::actingAs($user)->test('article-form', ['article' => $article])

            //assertSet verifica que una propiedad ya está seteada
            ->assertSet('article.title', $article->title)

            ->assertSet('article.slug', $article->slug)

            ->assertSet('article.content', $article->content)
            ->set('article.title', 'Updated title')
            ->set('article.slug', 'Updated-slug')
            ->call('save')
            ->assertSessionHas('status')
            ->assertRedirect(route('articles.index'));

            $this->assertDatabaseCount('articles', 1);

            $this->assertDatabaseHas('articles', [
                'title'=> 'Updated title',
                'slug'=> 'Updated-slug',
                'user_id'=> $user->id,
                
            
        ]);
        
       }

       /**
        * @test
        */

        function slug_is_generated_automatically()//funciona
        {
         Livewire::test('article-form')
         ->set('article.title', 'Nuevo articulo')
         ->assertSet('article.slug', 'nuevo-articulo');


        }
       

       


    // public function test_example(): void
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

   
}
