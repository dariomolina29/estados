<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Livewire\Component;



class ArticleForm extends Component
{

    public Article $article;

    protected function rules()
    {
        return[
            'article.title'=>['required', 'min:4'],
            'article.slug'=>[
            'required',
            'alpha_dash',
            Rule::unique('articles', 'slug')->ignore($this->article)
        ],
            'article.content'=>['required']
        ];
    }

    public function mount(Article $article)
    {
        $this->article = $article;
    }

    // Este método se va ejecutar cada vez que una propiedad de este componente se actualice. 
    // Permite realizar la validación de los campos en tiempo real sin recargar la página
    public function updated($propertyName) 
    {
      $this->validateOnly($propertyName);
    }

    public function updatedArticleTitle($title)
    {
        $this->article->slug = Str::slug($title);
    }

    public function save()
    {

      

        $this->validate();

        // Auth::user()->articles()->save($this->article); esta linea se hizo en reemplazo de las dos qui están abajo,
        //pero me muestra un error. Son complemento de 

       $this->article->user_id = auth()->id(); 

        $this->article->save();

        session()->flash('status', 'Artículo guardado.'); //Es la forma de mostrar al usuario el mensaje de que el artículo ha sido creado en la BD.
                                                        //Este mensaje se mostrará en el archivo "layouts/app.blade.php"

        $this->redirectRoute('articles.index');//Para redireccionar a la vista de listado después de crear el artículo
    }


    public function render()
    {
        return view('livewire.article-form');
    }
}


