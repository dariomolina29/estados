<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $guarded=[];

    //La siguiente función es para que se muestre el "slug" creado, en la url del navegador.
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
