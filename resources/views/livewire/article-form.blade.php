<div>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
           Crear nuevo
        </h2>
   
    </x-slot>

 

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">

             {{-- <x-jet-form-section>  --}}
            <x-form-section submit="save"> 
             
                <x-slot name="title">
                    {{ __('New article') }}
                </x-slot>  

                 <x-slot name="description">
                    {{ __('Some description') }}
                </x-slot>  
                
               
            

       <x-slot name="form">

        <div class="col-span-6">

        <x-label for="title" :value="('Título')"/>


        <x-input wire:model="article.title" id="title" class="mt-1" type="text"/>

        <x-input-error for="article.title" class="mt-2"/>

        
           

            </div>

            <div class="col-span-6">
       
            <label>
        <input wire:model="article.slug" type="text" placeholder="URL">
            @error('article.slug') <div>{{ $message }}</div> @enderror
        </label>
            </div>

            <div class="col-span-6">

        <label>
            <textarea wire:model="article.content" placeholder="Contenido"></textarea>
            @error('article.content') <div>{{ $message }}</div> @enderror
        </label>

            </div>

        
            <input type="submit" value="Guardar">


       </x-slot>

            </x-form-section>
        
  
</div>
</div>
</div>


